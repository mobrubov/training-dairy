<?php
use yii\helpers\Url;
use app\models\User;
// use yii\widgets\LinkPager;


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'New Exercise';

$form = ActiveForm::begin([
    'id' => 'new-programm-form',
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]);

$currentUser = \Yii::$app->user->getIdentity();
?>

<div class="site-index container">

	<h1><?= Html::encode($this->title) ?></h1>

    <?= $form->field($exercise, 'name') ?>
    <?= $form->field($exercise, 'approach_count') ?>
    <?= $form->field($exercise, 'repeats') ?>
    <?= $form->field($exercise, 'burdening') ?>
    <?= $form->field($exercise, 'description') ?>
    <?= $form->field($exercise, 'timing') ?>
    <?= Html::hiddenInput('program_id', $programId) ?>
    

    

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success', 'name' => 'new-programm-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
