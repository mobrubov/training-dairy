<?php

use yii\db\Schema;
use yii\db\Migration;

class m160505_194631_program_exercise extends Migration
{
    public function up()
    {
        $this->createTable('program_exercise', [
            'program_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'exercise_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function down()
    {
        echo "m160505_194631_program_exercise cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
