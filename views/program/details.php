<?php
use yii\helpers\Url;
use app\models\User;
// use yii\widgets\LinkPager;

$currentUser = \Yii::$app->user->getIdentity();
?>

<div class="btn-container">

    <h1><?php echo $programId ?></h1>

    <a class="button btn-info btn" href="<?= Url::toRoute(['program/create-exercise', 'program_id' => $programId])?>">Add New Exercise</a>
</div>

<div class="post-list-wrp">
    <?php if (count($exercises) > 0 ): ?>
        <table class="exercises-table table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Approach Count</th>
                    <th>Repeats</th>
                    <th>Additional Weight</th>
                    <th>Time</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($exercises as $_exercise): ?>
                    <tr>
                        <td><?= $_exercise->name ?></td>
                        <td><?= $_exercise->approach_count ?></td>
                        <td><?= $_exercise->repeats ?></td>
                        <td><?= $_exercise->burdening ?></td>
                        <td><?= $_exercise->timing ?></td>
                        <td><?= $_exercise->description ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    <?php else: ?>
        <span class="no-programs">There is no available exercises.</span>
    <?php endif;?>
</div>
