<?php

use yii\db\Schema;
use yii\db\Migration;

class m150830_071232_user extends Migration
{
    public function up()
    {
        $this->addColumn('db_user', 'is_admin', Schema::TYPE_BOOLEAN);
        $this->addColumn('db_user', 'avatar', Schema::TYPE_TEXT);
        $this->addColumn('db_user', 'email', Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m150830_071232_user cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
