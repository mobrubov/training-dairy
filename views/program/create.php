<?php
use yii\helpers\Url;
use app\models\User;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Новая программа';
?>

<h1><?= Html::encode($this->title) ?></h1>

<?php
    $form = ActiveForm::begin([
        'id' => 'new-programm-form',
        'options' => ['class' => 'custom-form hidden'],
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
            'labelOptions' => [
                // 'class' =>   'col-lg-1 control-label'
            ],
        ],
    ]);
?>

<?= $form->field($model, 'title') ?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-info', 'name' => 'new-programm-button']) ?>
</div>

<?php ActiveForm::end(); ?>

    <div class="row">
        <div class="col-lg-8">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="col-lg-1">#</th>
                    <th class="col-lg-6">Упражнение</th>
                    <th class="col-lg-1">Отягощение</th>
                    <th class="col-lg-1">Подходы</th>
                    <th class="col-lg-1">Повторения</th>
                    <th class="col-lg-2">Отдых между подходами</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Жим лежа</td>
                        <td>75</td>
                        <td>3</td>
                        <td>8</td>
                        <td>40 сек</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Швунг</td>
                        <td>45</td>
                        <td>3</td>
                        <td>12</td>
                        <td>20 сек</td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">
                            <div class="custom-table-button">
                                Добавить упражение
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
