<?php
namespace app\models;

use yii\db\ActiveRecord;

class Exercise extends ActiveRecord
{

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['approach_count', 'integer'],
            ['repeats', 'integer'],
            // TODO: check the datetime saving in a good format
            // ['timing', 'date', 'format' => 'H:m:s'],

            ['description', 'string', 'max' => 400],
//            [['title', 'text', 'parent_id'], 'required'],
//            ['parent_id', 'integer'],
        ];
    }


    public function getPrograms() {
        return $this->hasMany(Program::className(), ['program_id' => 'program_id'])
          ->viaTable('program_exercise', ['exercise_id' => 'exercise_id']);
    }


    public function create()
    {
        
        if ($this->validate() && $this->save(true)) {
            return true;
        } else {
            die('No record was created');
        }
    }

    public function getExerciseById($id)
    {
        $post = $this::find()
            ->where(['excercise_id' => $id])
            ->one();
        return $post;
    }


    public function deleteById($program_id)
    {
        $this::deleteAll('excercise_id = :id', [':id' => $program_id]);
    }

    private function getChildNodes()
    {

    }
}