<?php
/**
 * Created by PhpStorm.
 * User: mx
 * Date: 18.07.15
 * Time: 10:13
 */
namespace app\models;

use yii\db\ActiveRecord;

class Program extends ActiveRecord
{

    /**
     * user id attribut wich can be transmitted from post data
     */
    const USER_ID_ATTR_NAME = 'user_id';

    public function rules()
    {
        return [
            [['title'], 'required'],
//            [['title', 'text', 'parent_id'], 'required'],
//            ['parent_id', 'integer'],
        ];
    }


    public function getExercises() {
        return $this->hasMany(Exercise::className(), ['id' => 'exercise_id'])
          ->viaTable('program_exercise', ['program_id' => 'id']);
    }


    public function create()
    {
        $user = \Yii::$app->user->identity;
        $userId = $user ? $user->id : 0;

        if ($this->hasAttribute(self::USER_ID_ATTR_NAME)) {
            $this->setAttribute(self::USER_ID_ATTR_NAME, $userId);
        }

        if ($this->validate() && $this->save(true)) {
            return true;
        } else {
            die('No record was created');
        }
    }

    public function getProgramById($id)
    {
        $program = $this::find()
            ->where(['program_id' => $id])
            ->one();

        var_dump($id);
        return $program;
    }


    public function deleteById($program_id)
    {
        $this::deleteAll('program_id = :id', [':id' => $program_id]);
    }

    private function getChildNodes()
    {

    }
}