<?php

use yii\db\Schema;
use yii\db\Migration;

class m150723_213003_db_user extends Migration
{
    public function up()
    {
        $this->createTable('db_user', [
            'user_id' => Schema::TYPE_PK,
            'firstname' => Schema::TYPE_STRING . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_TEXT . ' NOT NULL',
            'authKey' => Schema::TYPE_STRING . ' NOT NULL',
            'rating' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function down()
    {
        echo "m150723_213003_db_user cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
