<?php
/**
 * Created by PhpStorm.
 * User: mx
 * Date: 21.07.15
 * Time: 4:30
 */
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class DbUser extends ActiveRecord
{
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['firstname'], 'alreadyExists'],
            ['email', 'yii\validators\EmailValidator'],
            ['password', 'required'],
        ];
    }
    
    public function register ()
    {
        if (isset($this['password'])) {
            $password = $this['password'];
            $this['password'] = Yii::$app->getSecurity()->generatePasswordHash($password);
            $this['authKey'] = Yii::$app->getSecurity()->generateRandomString();
            $this['is_admin'] = false;
        } else {
            return false;
        }
        if ($this->validate() && $this->save(true)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

        }
    }

    /**\
     *
     */
    public function alreadyExists()
    {

    }
}