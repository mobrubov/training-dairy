<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Program;
use app\models\Exercise;
use yii\helpers\Url;

class ProgramController extends Controller
{

    public function actionIndex()
    {
        $query = Program::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $programs = $query->orderBy(['created_at' => SORT_DESC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();


        $this->layout = '/clean';
        return $this->render('index', [
            'programs' => $programs,
            'pagination' => $pagination,
        ]);
    }

    public function actionCreate()
    {

        $model = new Program();
        $request = Yii::$app->request;

        if($request->getIsPost()) {
            $model->load($request->post());
            if ($model->create()) {
                return $this->redirect(Url::toRoute(['program/details', 'program_id' => $model->program_id]));
            };
        }
        $this->layout = '/clean';
        return $this->render('create', [
            'model' => $model
        ]);
    }

    public  function actionDelete()
    {
        $request = Yii::$app->request;
        if ($request->getIsGet()) {
            if ($postId = $request->get('post_id')) {
                $model = new Post();
                $model->deleteById($postId);
            }
        }
        return $this->goBack();
    }

    public  function actionEdit()
    {
        $model = new Post();
        $request = Yii::$app->request;
        if ($request->getIsPost()) {
            $model->load($request->post());
            if ($model->create()) {
                return $this->redirect(Url::toRoute('post/index'));
            };
        }
        $postId = $request->get('post_id');
        $postData = $model->getPostDataById($postId);
        if($request->getIsGet() && $request->get('post_id')) {
            $model->setAttributes($postData->getAttributes());
        }
        $this->layout = '/clean';
        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionDetails()
    {
        $query = Exercise::find();
        $request = Yii::$app->request;
        $programId = $request->get('program_id');

        // $exercises = $query->where('program_id = :id ', ['id'=>$programId])->all();
        $exercises = $query->all();


        $this->layout = '/clean';
        return $this->render('details', [
            'exercises' => $exercises,
            'programId' => $programId,
        ]);
    }


    public function actionCreateExercise()
    {
        $exercise = new Exercise();
        $request = Yii::$app->request;
        $programId = $request->get('program_id');

        if($request->getIsPost()) {
            $programId = $request->post('program_id');
            $programModel = new Program();
            $program = $programModel->getProgramById($programId);
            $exercise->load($request->post());

            if ($exercise->create()) {
                $exercise->link('programs', $program);
                return $this->redirect(Url::toRoute('program/details'));
            };
        }

        $this->layout = '/clean';

        return $this->render('exercise/create', [
            'exercise' => $exercise,
            'programId' => $programId,
        ]);
    }

}
