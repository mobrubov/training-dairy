<?php
use yii\helpers\Url;
use app\models\User;
use yii\widgets\LinkPager;
$currentUser = \Yii::$app->user->getIdentity();
?>


<h1>Мои программы</h1>

<?php if(!Yii::$app->user->isGuest): ?>
    <?php if (count($programs) > 0 ): ?>
        <ul class="post-list">
            <?php foreach ($programs as $_program): ?>
                <?php $url = Url::toRoute(['program/details', 'program_id' => $programId]); ?>
                <li><a href="<?= $url ?>"><?= $_program->title ?></a></li>
            <?php endforeach; ?>
        </ul>
        <?= LinkPager::widget(['pagination' => $pagination]) ?>
    <?php else: ?>
        <p class="no-programs">Программы не созданы</p>
    <?php endif;?>

    <div class="buttons-wrapper">
        <a class="btn btn-info" href="<?= Url::toRoute('program/create') ?>">Create New Program</a>
    </div>
<?php endif;?>
