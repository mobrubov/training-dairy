<?php
/**
 * Created by PhpStorm.
 * User: mx
 * Date: 30.08.15
 * Time: 10:22
 */
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\DbUser;
use app\models\ContactForm;
use yii\helpers\Url;

class AccountController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
//        if (!\Yii::$app->user->isGuest) {
//            return $this->redirect(Url::toRoute('account/index'));
//        } else {
//            $this->goBack();
//        }
        $this->layout = '/clean';
        return $this->render('index');
    }
}
