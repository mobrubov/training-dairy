<?php

use yii\db\Schema;
use yii\db\Migration;

class m150903_025904_programs extends Migration
{
    public function safeUp()
    {
        $this->createTable('program', [
            'program_id' => Schema::TYPE_PK,
            'user_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
            'title'      => Schema::TYPE_STRING . ' NOT NULL',
            'created_at'      => Schema::TYPE_DATETIME . ' NOT NULL',
        ]);

        $this->createTable('exercise', [
            'exercise_id' => Schema::TYPE_PK,
            'name'   => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_STRING . ' NOT NULL',
            'approach_count'   => Schema::TYPE_SMALLINT . ' NOT NULL',
            'repeats'         => Schema::TYPE_SMALLINT . ' NOT NULL',
            'burdening'   => Schema::TYPE_SMALLINT . ' NOT NULL',
            'timing'   => Schema::TYPE_SMALLINT . ' NOT NULL',
        ]);

        $this->insert('exercise', [
            'name' => 'Швунг',
            'description' => 'Поднятие штанги над головой в поожении стоя прямо',
            'approach_count' => 12,
            'repeats' => 3,
            'burdening' => 45
        ]);

        $this->insert('exercise', [
            'name' => 'Жим лежа',
            'description' => 'Поднятие штанги в положении лежа',
            'approach_count' => 8,
            'repeats' => 3,
            'burdening' => 75
        ]);

    }

    public function safeDown()
    {
        echo "m150903_025904_programs cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
