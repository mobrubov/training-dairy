<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = 'Training Dairy';
?>


    <?php if(!Yii::$app->user->isGuest): ?>

        <h1>Привет, <?php echo Yii::$app->user->identity->firstname ?></h1>
        <ul>
            <li><a href="<?= Url::toRoute('program/index') ?>">Мои программы</a></li>
            <li><a href="<?= Url::toRoute('program/index') ?>">Моя статистика</a></li>
            <li><a href="<?= Url::toRoute('program/index') ?>">Мои программы</a></li>
        </ul>
    <?php else: ?>
        <div class="info">
            Информация с данной страницы недоступна
        </div>
    <?php endif;?>
</div>
